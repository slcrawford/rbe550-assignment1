#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Python/ROS Introduction Assignment for WPI RBE550 Robot Motion Course
Created on Wed Jan 20 04:21:08 2021

@author: Stephen Crawford

"""

import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
import math
import time
#from std_srvs_srv import Empty


def main():
        
    print("Initial Yaw value: {:.4f}".format(yaw))
    relativeAngleInDegree = 180
    angularSpeedInDps = 60
    clockwise = False
    rotateTurtle(relativeAngleInDegree, angularSpeedInDps, clockwise)
    
    print("Final Yaw Value: {:.4f}".format(yaw))

    # speed = 1.0
    # distance = 6.0
    # goFwd = True
    # moveTurtle(speed, distance, goFwd)
    
        
def poseCallback(pose_message):
    global x, y, yaw
    
    x = pose_message.x
    y = pose_message.y
    yaw = pose_message.theta
    
    print("Current Pose (x, y, yaw): {:+.4f}, {:+.4f}, {:.2f}".format(x, y, math.degrees(yaw)))
    
    
def moveTurtle(speed, distance, goFwd):
    global x, y
    global velocity_publisher
    
    velocity_message = Twist()
    
    x_0 = x
    y_0 = y
    yaw_0 = yaw
    print("Yaw: {:.4f}".format(yaw_0))
    if (goFwd):
        velocity_message.linear.x = abs(speed)
    else:
        velocity_message.linear.x = -abs(speed)
    
    distance_moved = 0.0
    loop_rate =rospy.Rate(10)   # Publish velocity at 10Hz
    
    while True:
#        rospy.loginfo("Turtle moves forward")
        velocity_publisher.publish(velocity_message)
        
        loop_rate.sleep()
        
        distance_moved += abs(.5 * math.sqrt( ((x-x_0)**2) + ((y-y_0)**2) ) )  
        if not (distance_moved < distance):
            rospy.loginfo("Distance Reached")
            break
    # Finally stop the turtle when he reaches the distance
    velocity_message.linear.x = 0
    velocity_publisher.publish(velocity_message)


def rotateTurtle(relative_angle_degree, angular_speed_degree, clockwise):
    global yaw
    
    velocity_message = Twist()
    
    relative_angle_radians = abs(math.radians(relative_angle_degree))
    angular_speed_radians  = abs(math.radians(angular_speed_degree))

    if clockwise:
        velocity_message.angular.z = -abs(angular_speed_radians)
        relative_angle_radians = -relative_angle_radians
    else:
        velocity_message.angular.z =  abs(angular_speed_radians)
    
    # Turtle theta runs -180 -> 180 degrees (-pi -> pi) so
    # take current yaw and add 180 degrees to put in range of 0-360
    # now add the relative angle change for the desired theta(yaw)
    # use modulus 360 to keep/wrap the result within a single circle
    # finally subtract 180 degrees to bring the final result back to
    # the turtle's range of -180 to 180 degrees
    targetAngleInRadians = ((yaw + math.pi + relative_angle_radians) % (2*math.pi)) - math.pi
    
    # set a loop rate of 2*change for somewhat reliable monitoring of change
    loop_rate = rospy.Rate(2*int(angular_speed_degree))   
    while True:
        rospy.loginfo("Turtlesim begins a rotation")
        velocity_publisher.publish(velocity_message)
        
        deltaAngleInRadians = abs(yaw - targetAngleInRadians)
        loop_rate.sleep()
        
        if (deltaAngleInRadians < 0.01):
            rospy.loginfo("Angle {:.2f} reached".format(math.degrees(yaw)))
            break
        
    velocity_message.angular.z = 0
    velocity_publisher.publish(velocity_message)
   

if __name__ == "__main__":
    
    global x, y, yaw
    global distance_traveled, distance, speed
    global velocity_publisher
    global pose_subscriber

    print("TurtleDriver Module")
    
    try:    # to setup our turtle environment and interface
    
        rospy.init_node('turtlesim_motion_pose', anonymous=True)
        
        # Declare the turtle topics
        cmd_vel_topic = '/turtle1/cmd_vel'
        position_topic = '/turtle1/pose'
        
        # Setup the comms with the turtle
        velocity_publisher = rospy.Publisher(cmd_vel_topic, Twist, queue_size=10)
        pose_subscriber = rospy.Subscriber(position_topic, Pose, poseCallback)
        
        time.sleep(.1)

    except rospy.ROSInterruptException:
        rospy.loginfo("node terminated")
        print("ROS Node terminated, unable to establish ROS connections")
        exit(-1)
    main()
