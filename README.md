# RBE550-Assignment1
First assignment for class - Explore and Interact with a ROS Computational Graph
## Tasks
| Task |    Description                             Notes
|------|--------------------------------------|---------------------------------------------|
| 1 | Using ROS run TURTLESIM | Hint: Unsure what this is? Google Turtlesim
| 2 | Move the turtle using keyboard arrow<br>keys in 3 different shapes of your<br>choice. | Example of shapes: Star, hexagon - more than<br>5 edges|
| 3 | Echo the turtle's pose and velocity<br>while moving it. | Have 2 CLI windows visible |
| 4 | Show the ROS computational graph and<br>explain what you see. | EXTRA CREDIT TASK (5 Points): <br>Changing parameters: setting different<br>channel of background color, teleporting<br>the turtle |

                                                
### Globals:
 - x - Turtle's X Position
 - y - Turtle's Y Position
 - yaw - Turtle's rotation/angle
 - velocity_publisher - handle for turtle's velocity_publisher
 - pose_subcriber - handle for subscribing to turtle's position
    
### Functions
 - main()
 - poseCallback(pose_message)
 - moveTurtle(speed, distance, goFwd)
 - rotateTurtle(relative_angle_degree, angular_speed_degree, clockwise)
